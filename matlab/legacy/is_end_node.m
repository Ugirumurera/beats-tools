function [x]=is_end_node(N)
x = ~isfield(N,'outputs') || isempty(N.outputs) || isempty(N.outputs.output);
