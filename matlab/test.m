clear
close all

root = fileparts(fileparts(mfilename('fullpath')));

configfile = fullfile(root,'test_configs','twopath.xml');
output_requests_file = fullfile(root,'test_configs','twopath_output_request.xml');
duration = 3600;
prefix = 'beatstest';
output_folder = tempdir;
 
beats = BeATSWrapper(configfile);
beats.run_simulation(prefix,output_requests_file,output_folder,duration)
beats.animate
