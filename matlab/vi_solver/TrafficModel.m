classdef (Abstract) TrafficModel < handle
    
    properties
        beats @BeATSWrapper
        
    end
    
    methods(Access=public)
        
        function this = TrafficModel(configfile)
            this.beats = BeATSWrapper(configfile);
        end
        
        function demand_costs = evaluate_cost(model,cost_function);
            link_states = model.run_demand(model.beats.demands_on_paths);
            link_costs = cost_function.evaluate(link_states);
            demand_costs = model.get_incidence_matrix * link_cost;
        end
        
    end
    
    methods (Abstract)
        run_demand
        
        gradient(this);
    end

end

