classdef VehicleEvent < Event
    
    properties(Access=public)
        vehicle
        from_lanegroup
        from_queue
        to_lanegroup
        to_queue
    end
    
    methods
        
        function this = VehicleEvent(time,items)
            this = this@Event(time);
            if nargin<2
                return
            end
            this.vehicle = uint32(str2double(items{1}));
            if strcmp(items{2},'-')
                this.from_queue = '';
                this.from_lanegroup = nan;
            else
                this.from_queue = items{2}(1);
                this.from_lanegroup = uint16(str2double(items{2}(2:end)));
            end
            if strcmp(items{3},'-')
                this.to_queue = '';
                this.to_lanegroup = nan;
            else
                this.to_queue = items{3}(1);
                this.to_lanegroup = uint16(str2double(items{3}(2:end)));
            end
        end
        
    end
    
end

