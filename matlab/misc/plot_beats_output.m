function [cfg,X]=plot_beats_output(cfg_file,output_folder,ppt_folder)

do_ppt = nargin>=3;

X=xml_read(cfg_file);
cfg.output_requests = X.output_requests;
cfg.commodities = X.commodities;
if isfield(X,'subnetworks')
    cfg.subnetworks = X.subnetworks;
else
    cfg.subnetworks = [];
end
clear X
                
for k=1:length(cfg.output_requests.output_request)
    
    fprintf('%d of %d\n',k,length(cfg.output_requests.output_request))
    
    z = cfg.output_requests.output_request(k).ATTRIBUTE;
    
    if isempty(strfind(z.quantity,'lanegroup')) && isempty(strfind(z.quantity,'link'))
        continue
    end
    
    is_lanegroup = ~isempty(strfind(z.quantity,'lanegroup'));   % lanegroup or link
    is_flw = ~isempty(strfind(z.quantity,'flw'));               % flw or veh
    
    % commodity and subnetwork ids
    if isfield(z,'commodity_id')
        commodity_id = num2str(z.commodity_id);
    else
        commodity_id = 'g';
    end
    
    if isfield(z,'subnetwork_id')
        subnetwork_id = num2str(z.subnetwork_id);
    else
        subnetwork_id = 'g'; %get_subnetwork_for_commodity(cfg,commodity_id);
    end
    
    % read the data
    splitprefix = strsplit(z.prefix,'/');
    filename = fullfile(output_folder,sprintf('%s_%d_%s_%s_%s',splitprefix{end},z.dt,z.quantity,commodity_id,subnetwork_id));
    x.time = load([filename '_time.txt']);
    x.data = load([filename '.txt']);
    if is_lanegroup
        x.items = read_lanegroups([filename '_lanegroups.txt']);
    else
        x.items = load([filename '_links.txt']);
    end
    
    % plot to powerpoint
    if do_ppt
        ppt_file = sprintf('plots_%d_%s_%s_%s',z.dt,z.quantity,commodity_id,subnetwork_id);
        [ppt,op]=openppt(fullfile(ppt_folder,ppt_file),true);
    end
    
    for i=1:length(x.items)
            
        if do_ppt
            figure('Visible','off')
        else
            figure
        end

        if is_flw
            dt = mean(diff(x.time))/3600;
            plot(x.time(1:end-1),smooth(diff(x.data(:,i))/dt),'LineWidth',2)
            ylabel('flow')
        else
            plot(x.time,smooth(x.data(:,i)),'LineWidth',2)
            ylabel('veh')
        end
        set(gca,'XLim',[0 max(x.time)])
        grid
            
        if is_lanegroup
            tit = sprintf('commodity %s, link %d, lanes %d-%d',commodity_id,x.items(i).link,x.items(i).start_lane,x.items(i).end_lane);
        else
            tit = sprintf('commodity %s, link %d',commodity_id,x.items(i));
        end
        
        if do_ppt
            addslide(op,tit,[],[],[],0.5)
            close
        else
            title(tit)
        end
        
    end
    
    if do_ppt
        closeppt(ppt,op)
    end
    
    % store
    X(k) = struct('time',x.time, ...
                  'data',x.data, ...
                  'items',x.items, ...
                  'is_lanegroup',is_lanegroup , ...
                  'is_flw', is_flw , ...
                  'commodity_id' , commodity_id , ...
                  'subnetwork_id' , subnetwork_id );
   
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [lanegroups]=read_lanegroups(filename)

A= load(filename);
n = size(A,1);
lanegroups = repmat(struct('id',nan,'link',nan,'start_lane',nan,'end_lane',nan),1,n);
for i=1:n
    lanegroups(i).id = A(i,1);
    lanegroups(i).link = A(i,2);
    lanegroups(i).start_lane = A(i,3);
    lanegroups(i).end_lane = A(i,4);
end


function [subnetwork_id]=get_subnetwork_for_commodity(cfg,commodity_id)
if strcmp(commodity_id,'g')
    subnetwork_id = 'g';
    return
end
commodity_id = str2double(commodity_id);
foundit = false;
for i=1:length(cfg.commodities.commodity)
    if cfg.commodities.commodity(i).ATTRIBUTE.id==commodity_id
        foundit=true;
        comm = cfg.commodities.commodity(i).ATTRIBUTE;
        break
    end
end
if foundit
    if isfield(comm,'subnetworks')
        subnetwork_id = num2str(comm.subnetworks);
    else
        subnetwork_id = '0';
    end
end